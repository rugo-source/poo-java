package herencia;

public class Consultor extends Persona{
    String nombreConsultor;
    int numeroConsultor;

    public Consultor() {
    }

    public Consultor(int id, String dni, String name, String apellido, String domicilio, String telefono, String nombreConsultor, int numeroConsultor) {
        super(id, dni, name, apellido, domicilio, telefono);
        this.nombreConsultor = nombreConsultor;
        this.numeroConsultor = numeroConsultor;
    }

    public String getNombreConsultor() {
        return nombreConsultor;
    }

    public void setNombreConsultor(String nombreConsultor) {
        this.nombreConsultor = nombreConsultor;
    }

    public int getNumeroConsultor() {
        return numeroConsultor;
    }

    public void setNumeroConsultor(int numeroConsultor) {
        this.numeroConsultor = numeroConsultor;
    }
}
