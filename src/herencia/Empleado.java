package herencia;

public class Empleado extends Persona{

    int numero;
    String cargo;
    Double sueldo;

    public Empleado() {
    }

    public Empleado(int id, String dni, String name, String apellido, String domicilio, String telefono, int numero, String cargo, Double sueldo) {
        super(id, dni, name, apellido, domicilio, telefono);
        this.numero = numero;
        this.cargo = cargo;
        this.sueldo = sueldo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Double getSueldo() {
        return sueldo;
    }

    public void setSueldo(Double sueldo) {
        this.sueldo = sueldo;
    }
}
