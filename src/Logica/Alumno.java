package Logica;

public class Alumno {
    Integer id;
    String nombre;
    String apellido;

    public Alumno() {//constuctor vacio
    }

    public Alumno(Integer id, String nombre, String apellido) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void mostrarNombre(){
        System.out.println("Soy alumno y se decir mi nombre");
    }
    public void saberAprobado(Double calificacion){
        if (calificacion>= 6){
            System.out.println("aprobe la materia");
        }else {
            System.out.println("reprobe la materia");
        }
    }


}
