package classabstractas;

public class Circulo implements figura,Dibujable,Rotables {
    private double radios;

    public Circulo() {
    }

    public Circulo( double radios) {
        this.radios = radios;
    }

    @Override
    public double calcularArea() {
        return 3.14 * radios * radios;
    }

    @Override
    public void dibujar() {
        System.out.println("estoy dibujando");
    }

    @Override
    public void rotar() {
        System.out.println("estoy rotando");
    }
}
