package classabstractas;

public class Cuadrado implements figura,Dibujable{
    private double lado;

    public Cuadrado() {
    }


    public Cuadrado( double lado) {
        this.lado = lado;
    }

    @Override
    public double calcularArea() {
        return lado *lado;
    }

    @Override
    public void dibujar() {
        System.out.println(" estoy dibujando");
    }


}
